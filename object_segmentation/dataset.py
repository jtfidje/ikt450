import json
import os
import numpy as np
import torch
from PIL import Image
import skimage.draw


class BalloonDataset(object):
    def __init__(self, root, transforms):
        self.root = root
        self.transforms = transforms
        
        with open(os.path.join(root, 'via_region_data.json'), 'r') as f:
            annotations = json.load(f)
        
        self.annotations = [v for v in annotations.values() if v['regions']]

    def create_mask(self, regions, height, width):
        polygons = [r['shape_attributes'] for r in regions.values()]
        mask = np.zeros([height, width, len(polygons)], dtype=np.uint8)
        for i, p in enumerate(polygons):
            rr, cc = skimage.draw.polygon(p['all_points_y'], p['all_points_x'])
            mask[rr, cc, i] = 1

        return mask.astype(np.bool)

    def __getitem__(self, idx):
        # load images ad masks
        img_info = self.annotations[idx]
        
        img_path = os.path.join(self.root, img_info['filename'])
        img = Image.open(img_path).convert("RGB")
        
        masks = self.create_mask(img_info['regions'], img.height, img.width)

        # get bounding box coordinates for each mask
        num_objs = masks.shape[-1]
        boxes = []
        for i in range(num_objs):
            pos = np.where(masks[:, :, i])
            xmin = np.min(pos[1])
            xmax = np.max(pos[1])
            ymin = np.min(pos[0])
            ymax = np.max(pos[0])
            boxes.append([xmin, ymin, xmax, ymax])

        # convert everything into a torch.Tensor
        boxes = torch.as_tensor(boxes, dtype=torch.float32)
        # there is only one class
        labels = torch.ones((num_objs,), dtype=torch.int64)
        masks = torch.as_tensor(masks, dtype=torch.uint8)
        masks = masks.permute(2, 0, 1)

        image_id = torch.tensor([idx])
        area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
        # suppose all instances are not crowd
        iscrowd = torch.zeros((num_objs,), dtype=torch.int64)

        target = {}
        target["boxes"] = boxes
        target["labels"] = labels
        target["masks"] = masks
        target["image_id"] = image_id
        target["area"] = area
        target["iscrowd"] = iscrowd

        if self.transforms is not None:
            img, target = self.transforms(img, target)

        return img, target

    def __len__(self):
        return len(self.annotations)